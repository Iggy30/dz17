﻿#include <iostream>
using namespace std;

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:

    Vector(float valueX, float valueY, float valueZ)
    {
        x = valueX;
        y = valueY;
        z = valueZ;
    }

      
    void Show()
    {
        cout << "Длина вектора равна :"<< sqrt(x * x + y * y + z * z);
    }

};

int main()
{
    setlocale(LC_ALL, "Rus");

    cout << "Введите координаты оси X,Y,Z:";
    float x;
    cin >> x;
    float y;
    cin >> y;
    float z;
    cin >> z;
    Vector v(x, y, z);
    v.Show();

}